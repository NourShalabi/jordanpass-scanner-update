package com.jordanpass.jordanpass.library;

public class Pass {
	// private variables
	int id;
	int type;
	String expiryDate;
	String scanTime;

	public String GetFirstScanTime() {
		return firstScanTime;
	}

	String firstScanTime;
	int locationId;
	long expiryDateInMillis;
	int numberOfScans = 1;

	// Empty constructor
	public Pass(int id, int type, String expireyDate, String scanTime) {
		// TODO get locationId from app settings
		this.id = id;
		this.type = type;
		this.expiryDate = expireyDate;
		this.scanTime = scanTime;
		this.numberOfScans= 1; 
	}
	
	public Pass(int id, int type, long expireyDate, String scanTime, int numberOfScans) {
		// TODO get locationId from app settings
		this.id = id;
		this.type = type;
		this.expiryDateInMillis = expireyDate;
		this.scanTime = scanTime;
		this.numberOfScans= numberOfScans;
	}

	public Pass(int id, int type, long expireyDate, String scanTime, String firstScanTime, int numberOfScans) {
		// TODO get locationId from app settings
		this.id = id;
		this.type = type;
		this.expiryDateInMillis = expireyDate;
		this.scanTime = scanTime;
		this.numberOfScans= numberOfScans;
		this.firstScanTime = firstScanTime;
	}

	public Pass(int id, int type, String expireyDate, String scanTime,
			int locationId) {
		// TODO get locationId from app settings
		this.id = id;
		this.type = type;
		this.expiryDate = expireyDate;
		this.scanTime = scanTime;
		this.locationId = locationId;
		this.numberOfScans= 1;
	}

	public int GetPassId() {
		return id;
	}

	public int GetPassType() {
		return type;
	}

	public String GetExpiryDate() {
		return expiryDate;
	}
	
	public long GetExpiryDateInMillis() {
		return expiryDateInMillis;
	}

	public String GetScanTime() {
		return scanTime;
	}

	public int GetLocationId() {
		return locationId;
	}
	
	public int GetNumberOfScans() {
		return numberOfScans;
	}
}
