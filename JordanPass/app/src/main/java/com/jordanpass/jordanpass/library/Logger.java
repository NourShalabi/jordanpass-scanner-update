package com.jordanpass.jordanpass.library;

import android.util.Log;

public class Logger {
	
	Boolean loggerOn;
	String tag;
	public Logger(String tag) {
		this.tag = tag;
		loggerOn = true;
		if (loggerOn)
		Log.i(tag, String.format("Logger with tag %s started", tag));
	}
	
	public void e(String msg){
		if (loggerOn) Log.e(tag, msg);
	}
	
	public void w(String msg){
		if (loggerOn) Log.w(tag, msg);
	}
	
	public void i(String msg){
		if (loggerOn) Log.i(tag, msg);
	}
	
	public void d(String msg){
		if (loggerOn) Log.d(tag, msg);
	}

}
