/*
 * Basic no frills app which integrates the ZBar barcode scanner with
 * the camera.
 * 
 * Created by lisah0 on 2012-02-24
 */
package com.jordanpass.jordanpass;

import com.jordanpass.jordanpass.library.CameraPreview;
import com.jordanpass.jordanpass.library.DatabaseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.Button;

import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;

import android.widget.TextView;
/* Import ZBar Class files */
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import net.sourceforge.zbar.Config;

import java.util.List;

public class MainScannerActivity extends Activity {
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;

	TextView scanText;
	Button scanButton;

	ImageScanner scanner;

	private boolean barcodeScanned = false;
	private boolean previewing = true;

	static {
		System.loadLibrary("iconv");
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_scanner);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		/* Instance barcode scanner */
		scanner = new ImageScanner();

		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);

		// Only enable the codes your app requires
		scanner.setConfig(Symbol.QRCODE, Config.ENABLE, 1);

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);

		scanText = (TextView) findViewById(R.id.scanText);

		scanButton = (Button) findViewById(R.id.ScanButton);

		scanButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (barcodeScanned) {
					barcodeScanned = false;
					scanText.setText("Scanning...");
					mCamera.setPreviewCallback(previewCb);
					mCamera.startPreview();
					previewing = true;
					mCamera.autoFocus(autoFocusCB);
				}
			}
		});
	}

	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	public void onResume() {
		super.onResume();
		// releaseCamera();
		if (mCamera == null) {
			mCamera = getCameraInstance();
			mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
			FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
			preview.removeAllViews();
			preview.addView(mPreview);
			Camera.Parameters p = mCamera.getParameters();
			List<String> focusModes = p.getSupportedFocusModes();

			if(focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
				//Phone supports autofocus!
				mCamera.autoFocus(autoFocusCB);
			}

			if (barcodeScanned) {
				barcodeScanned = false;
				scanText.setText("Scanning...");
				mCamera.setPreviewCallback(previewCb);
				mCamera.startPreview();
				previewing = true;
				mCamera.autoFocus(autoFocusCB);
			}
		}
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				SymbolSet syms = scanner.getResults();
				// for (Symbol sym : syms) {
				// Get the first scanned code and send it to ResultActivity.
				if (syms.size() > 0) {
					Symbol sym = (Symbol) syms.toArray()[0];
					scanText.setText("barcode result " + sym.getData());
					Log.e("JP", sym.getData());
					barcodeScanned = true;

					Intent resultActivity = new Intent(
							MainScannerActivity.this, ResultActivity.class);
					resultActivity.putExtra("result", sym.getData());
					startActivity(resultActivity);
					finish();
				}
			}
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_scanner, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		// respond to menu item selection
		switch (item.getItemId()) {
		case R.id.changeLoc:

			AlertDialog.Builder locBuilder = new AlertDialog.Builder(this);
			locBuilder
					.setMessage(
							"Are you sure you want to change your location settings? You will not be able to scan tickets until an admin approves your location change request.")
					.setCancelable(true)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									startActivity(new Intent(
											MainScannerActivity.this,
											RequestApprovalActivity.class));
									finish();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog locAlert = locBuilder.create();
			locAlert.show();
			return true;
		case R.id.sync:
			DatabaseHandler dbHandler = new DatabaseHandler(this);
			Integer itemCount = dbHandler.getUnsyncedPassesCount();
			String msg = itemCount.intValue() == 0 ? "No records to sync."
					: (itemCount.intValue() == 1 ? "A record is being synced."
							: itemCount + " records are being synced.");

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
					.setCancelable(true)
					.setNeutralButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// TODO nothing I assume
								}
							});
			AlertDialog alert = builder.create();
			alert.show();

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
