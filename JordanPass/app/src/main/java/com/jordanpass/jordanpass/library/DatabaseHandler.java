package com.jordanpass.jordanpass.library;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 5;

	// Database Name
	private static final String DATABASE_NAME = "JordanPassDB";

	// Passes table name
	private static final String TABLE_PASSES = "ScannedPasses";
	private static final String TABLE_ALL_PASSES = "AllPasses";
	private static final String TABLE_ERROR_LOGS = "ErrorLogs";

	// Passes Table Columns names

	private static final String KEY_ID = "PassId";
	private static final String KEY_TYPE = "PassType";
	private static final String KEY_EXPIRY_DATE = "ExpiryDate";
	private static final String KEY_SCAN_TIME = "ScanTime";
    private static final String KEY_FIRST_SCAN_TIME = "FirstScanTime";
	private static final String KEY_LOCATION = "LocationId";
	private static final String KEY_NUMBER_OF_SCANS = "NumberOfScans";

	//Error Logs Table Columns Names
	private static final String LOG_ID = "LogId";
	private static final String ERROR_ID = "ErrorId";
	private static final String ERROR_DATE = "ErrorDate";
	private static final String LOCATION_ID = "LocationId";
	private static final String EXTRA_INFO = "ExtraInfo";


	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_PASSES_TABLE = "CREATE TABLE " + TABLE_PASSES + "("
				+ "Id" + " INTEGER PRIMARY KEY," + KEY_ID + " INTEGER,"
				+ KEY_TYPE + " INTEGER," + KEY_EXPIRY_DATE + " TEXT, "
				+ KEY_SCAN_TIME + " TEXT, " + KEY_LOCATION + " INT " + ")";
		db.execSQL(CREATE_PASSES_TABLE);
		
		String CREATE_ALL_PASSES_TABLE = "CREATE TABLE " + TABLE_ALL_PASSES + "("
				+ "Id" + " INTEGER PRIMARY KEY," + KEY_ID + " INTEGER,"
				+ KEY_TYPE + " INTEGER," + KEY_EXPIRY_DATE + " INTEGER, "
                + KEY_SCAN_TIME + " TEXT, " + KEY_FIRST_SCAN_TIME + " TEXT, " + KEY_NUMBER_OF_SCANS
				+ " INTEGER DEFAULT 1 " + ")";
		db.execSQL(CREATE_ALL_PASSES_TABLE);

		String CREATE_ERROR_LOGS_TABLE = "CREATE TABLE " + TABLE_ERROR_LOGS + "("
				+ LOG_ID + " INTEGER PRIMARY KEY,"	+ ERROR_ID + " INTEGER," + LOCATION_ID + " INTEGER, "
				+ ERROR_DATE + " TEXT, " + EXTRA_INFO + " TEXT " + ")";
		db.execSQL(CREATE_ERROR_LOGS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALL_PASSES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PASSES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ERROR_LOGS);

		String CREATE_PASSES_TABLE = "CREATE TABLE " + TABLE_ALL_PASSES + "("
				+ "Id" + " INTEGER PRIMARY KEY," + KEY_ID + " INTEGER,"
				+ KEY_TYPE + " INTEGER," + KEY_EXPIRY_DATE + " INTEGER, "
				+ KEY_SCAN_TIME + " TEXT, "+ KEY_FIRST_SCAN_TIME + " TEXT, " + KEY_NUMBER_OF_SCANS
				+ " INTEGER DEFAULT 1 " + ")";
		db.execSQL(CREATE_PASSES_TABLE);
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PASSES);
		 CREATE_PASSES_TABLE = "CREATE TABLE " + TABLE_PASSES + "("
				+ "Id" + " INTEGER PRIMARY KEY," + KEY_ID + " INTEGER,"
				+ KEY_TYPE + " INTEGER," + KEY_EXPIRY_DATE + " TEXT, "
				+ KEY_SCAN_TIME + " TEXT, " + KEY_LOCATION + " INT " + ")";
		db.execSQL(CREATE_PASSES_TABLE);

		String CREATE_ERROR_LOGS_TABLE = "CREATE TABLE " + TABLE_ERROR_LOGS + "("
				+ LOG_ID + " INTEGER PRIMARY KEY,"	+ ERROR_ID + " INTEGER," + LOCATION_ID + " INTEGER, "
				+ ERROR_DATE + " TEXT, " + EXTRA_INFO + " TEXT " + ")";
		db.execSQL(CREATE_ERROR_LOGS_TABLE);

	
	}

	public void addPass(Pass pass) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ID, pass.GetPassId());
		values.put(KEY_TYPE, pass.GetPassType());
		values.put(KEY_EXPIRY_DATE, pass.GetExpiryDate());
		values.put(KEY_SCAN_TIME, pass.GetScanTime());
		values.put(KEY_LOCATION, pass.GetLocationId());

		db.insert(TABLE_PASSES, null, values);
		db.close(); // Closing database connection
	}

	public void addPassToAllPasses(int passId, int passType, long expiryDate,
			String scanTime) {
		
		Pass testPass = getPassByIdFromAllPasses(passId);
		if (testPass == null) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_ID, passId);
			values.put(KEY_TYPE, passType);
			values.put(KEY_EXPIRY_DATE, expiryDate);
			values.put(KEY_SCAN_TIME, scanTime);
            values.put(KEY_FIRST_SCAN_TIME, scanTime);
			db.insert(TABLE_ALL_PASSES, null, values);
			db.close(); // Closing database connection
		} else {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues newValues = new ContentValues();

            newValues.put(KEY_SCAN_TIME,
                    scanTime);


			try {
				SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				resetCalendarTime(cal);
				Date lastScan = dbDateFormat.parse(testPass.GetScanTime().split(" ")[0]);
				Date scanTimeDate = dbDateFormat.parse(scanTime.split(" ")[0]);
				if(!(lastScan.compareTo(scanTimeDate) == 0)){
					newValues.put(KEY_NUMBER_OF_SCANS,
							testPass.GetNumberOfScans() + 1);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				newValues.put(KEY_NUMBER_OF_SCANS,
						testPass.GetNumberOfScans() + 1);
			}

			db.update(TABLE_ALL_PASSES, newValues, KEY_ID + " = " + passId,
					null);
			db.close();
		}
	}

	private void resetCalendarTime(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	public Pass getPassByIdFromAllPasses(int id) {
		//getExpiredPasses();
		SQLiteDatabase db = this.getReadableDatabase();
		Pass pass = null;
		Cursor cursor = db.query(TABLE_ALL_PASSES,
				new String[] { KEY_ID, KEY_TYPE, KEY_EXPIRY_DATE,
                        KEY_SCAN_TIME, KEY_FIRST_SCAN_TIME, KEY_NUMBER_OF_SCANS }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			if (cursor.getCount() > 0) {
				pass = new Pass(Integer.parseInt(cursor.getString(0)),
						Integer.parseInt(cursor.getString(1)),
						Long.parseLong(cursor.getString(2)),
						cursor.getString(3),cursor.getString(4),  Integer.parseInt(cursor
								.getString(5)));
				Log.e("DB", String.format("PASS: %s \n %s \n %s \n %s \n %s",
						cursor.getString(0), cursor.getString(1),
						cursor.getString(2), cursor.getString(3),
						cursor.getString(4)));
                SimpleDateFormat formatter = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss");
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, -5);
                Log.e("DB", formatter.format(calendar.getTime()));
			}
		}
		db.close();
		return pass;
	}

	public List<Pass> getExpiredPasses() {
		List<Pass> passList = new ArrayList<Pass>();
		// Select All Query
		Calendar calendar = Calendar.getInstance();
		String selectQuery = "SELECT  * FROM " + TABLE_ALL_PASSES + " WHERE "
				+ KEY_EXPIRY_DATE + " < " + calendar.getTimeInMillis();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Pass pass = new Pass(Integer.parseInt(cursor.getString(1)),
						Integer.parseInt(cursor.getString(2)),
						Long.parseLong(cursor.getString(3)),
						cursor.getString(4), Integer.parseInt(cursor
								.getString(6)));
				passList.add(pass);
			} while (cursor.moveToNext());
		}

		db.close();
		return passList;
	}

	public Pass getPassById(int id) {

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PASSES, new String[] { KEY_ID, KEY_TYPE,
				KEY_EXPIRY_DATE, KEY_SCAN_TIME, KEY_LOCATION }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Pass pass = new Pass(Integer.parseInt(cursor.getString(0)),
				Integer.parseInt(cursor.getString(1)), cursor.getString(2),
				cursor.getString(3), Integer.parseInt(cursor.getString(4)));
		db.close();
		return pass;
	}

	public Integer checkPassById(int id) {

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PASSES, new String[] { KEY_ID, KEY_TYPE,
				KEY_EXPIRY_DATE, KEY_SCAN_TIME, KEY_LOCATION }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		List<Pass> passList = new ArrayList<Pass>();
		if (cursor.moveToFirst()) {
			do {
				Pass pass = new Pass(Integer.parseInt(cursor.getString(0)),
						Integer.parseInt(cursor.getString(1)),
						cursor.getString(2), cursor.getString(3),
						Integer.parseInt(cursor.getString(4)));
				passList.add(pass);
			} while (cursor.moveToNext());
		}
		db.close();
		return passList.size();
	}
	
	

	public List<Pass> getUnsyncedPasses() {
		List<Pass> passList = new ArrayList<Pass>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_PASSES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Pass pass = new Pass(Integer.parseInt(cursor.getString(1)),
						Integer.parseInt(cursor.getString(2)),
						cursor.getString(3), cursor.getString(4),
						Integer.parseInt(cursor.getString(5)));
				passList.add(pass);
			} while (cursor.moveToNext());
		}
		db.close();
		return passList;
	}

	public int getUnsyncedPassesCount() {
		String countQuery = "SELECT  * FROM " + TABLE_PASSES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int n = cursor.getCount();
		cursor.close();
		db.close();
		// return count
		return n;
	}

	// TODO do we need that?
	/*
	 * public int updatePass(Pass pass) { }
	 */

	public void deletePass(Pass pass) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PASSES, KEY_ID + " = ?",
				new String[] { String.valueOf(pass.GetPassId()) });
		db.close();
	}
	
	public void deleteExpiredPasses() {
		SQLiteDatabase db = this.getWritableDatabase();
		Calendar calendar = Calendar.getInstance();
		db.execSQL("DELETE FROM " + TABLE_ALL_PASSES + " WHERE "
                + KEY_EXPIRY_DATE + " < " + calendar.getTimeInMillis());
		db.close();
	}

    public void deleteAllPasses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_ALL_PASSES);
        db.close();
    }

    public void deleteAllUnsyncedPasses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PASSES);
        db.close();
    }


	public void addErrorLog(ErrorLog errorLog) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ERROR_ID, errorLog.getErrorId());
		values.put(ERROR_DATE, errorLog.getErrorDate());
		values.put(LOCATION_ID, errorLog.getLocationId());
		values.put(EXTRA_INFO, errorLog.getExtraInfo());

		db.insert(TABLE_ERROR_LOGS, null, values);
		db.close(); // Closing database connection
	}

	public int getUnsyncedErrorLogsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_ERROR_LOGS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int n = cursor.getCount();
		cursor.close();
		db.close();
		// return count
		return n;
	}


	public List<ErrorLog> getUnsyncedErrorLogs() {
		List<ErrorLog> errorLogList = new ArrayList<>();
		// Select All Query
		String selectQuery = String.format("SELECT %s, %s, %s, %s FROM %s", ERROR_ID, LOCATION_ID, ERROR_DATE, EXTRA_INFO, TABLE_ERROR_LOGS);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ErrorLog errorLog = new ErrorLog(Integer.parseInt(cursor.getString(0)),
						Integer.parseInt(cursor.getString(1)),
						cursor.getString(2), cursor.getString(3));
				errorLogList.add(errorLog);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return errorLogList;
	}

	public void deleteAllUnsyncedErrorLogs() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_ERROR_LOGS);
		db.close();
	}

}