package com.jordanpass.jordanpass;

import com.jordanpass.jordanpass.library.ApprovalStatus;
import com.jordanpass.jordanpass.library.Preferences;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ApprovalPendingActivity extends Activity {

	Button retryBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_approval_pending);

		retryBtn = (Button) findViewById(R.id.button1);
		retryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences preferences = getSharedPreferences(
						Preferences.APP_SETTINGS, MODE_PRIVATE);
				SharedPreferences.Editor edit = preferences.edit();
				edit.putInt(Preferences.APPROVAL_STATUS,
						ApprovalStatus.NOT_APPROVED);
				edit.commit(); // commit changes to app preferences
				startActivity(new Intent(ApprovalPendingActivity.this,
						RequestApprovalActivity.class));
				finish();
			}
		});
	}

	BroadcastReceiver requestResultReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Boolean message = intent.getBooleanExtra("result", false);
			if (message) {
				startActivity(new Intent(ApprovalPendingActivity.this,
						LaserScannerActivity.class));
				finish();
			} else {
				startActivity(new Intent(ApprovalPendingActivity.this,
						RequestApprovalActivity.class));
				finish();
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();

		// Register mMessageReceiver to receive messages.
		LocalBroadcastManager.getInstance(this).registerReceiver(
				requestResultReceiver, new IntentFilter("GCMMessage"));
	}

	@Override
	protected void onPause() {
		// Unregister since the activity is not visible
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				requestResultReceiver);
		super.onPause();
	}
}
