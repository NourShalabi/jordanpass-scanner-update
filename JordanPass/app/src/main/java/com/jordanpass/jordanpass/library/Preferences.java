package com.jordanpass.jordanpass.library;

public class Preferences {

	public static final String APP_SETTINGS = "AppSettings";
	public static final String FIRST_RUN = "isFirstRun";
	public static final String APPROVAL_STATUS = "ApprovalStatus";
	public static final String APPROVAL_TIME = "ApprovalTime";
	public static final String GCM_REG_ID = "GCMRegId";
	public static final String DEVICE_ID = "DeviceId";
	public static final String LOCATION_ID = "LocationId";
	
	public static final String GCM_KEY = "834602759271";
	
}
