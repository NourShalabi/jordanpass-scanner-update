package com.jordanpass.jordanpass;

import com.jordanpass.jordanpass.library.DatabaseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

public class LaserScannerActivity extends Activity {
	ClipboardManager clipboard;
	OnPrimaryClipChangedListener listener;

	EditText editText;
	private String scannedString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_laser_scanner);
		
		 clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		/*final OnPrimaryClipChangedListener listener= new OnPrimaryClipChangedListener() {
			
			@Override
			public void onPrimaryClipChanged() {
				// TODO Auto-generated method stub
				String data = clipboard.getPrimaryClip().getItemAt(0).getText().toString();
				
				Log.e("JP", data);
				Intent resultActivity = new Intent(
						LaserScannerActivity.this, ResultActivity.class);
				resultActivity.putExtra("result", data);
				clipboard.removePrimaryClipChangedListener(this);
				startActivity(resultActivity);
				finish();
			}
		};
		clipboard.addPrimaryClipChangedListener(listener);
		*/

		editText = (EditText) findViewById(R.id.editText);
		editText.requestFocus();

		final TextWatcher filterTextWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				Log.e("text", editText.getText().toString());
				editText.removeTextChangedListener(this);
				//findResult(editText.getText().toString());
				scannedString = editText.getText().toString();


				ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText("q", scannedString);
				clipboard.setPrimaryClip(clip);

	/*			Intent resultActivity = new Intent(
						LaserScannerActivity.this, ResultActivity.class);
				resultActivity.putExtra("result", scannedString);
*/
				Log.e("scannedString", scannedString);
				editText.setText("");
				editText.addTextChangedListener(this);
				//startActivity(resultActivity);
			}
		};

		editText.addTextChangedListener(filterTextWatcher);
	}





	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_scanner, menu);
		return true;
	}
	
	public void onPause() {
		super.onPause();
		if (clipboard != null && listener!= null){
			clipboard.removePrimaryClipChangedListener(listener);
		}
		
	}

	public void onResume() {
		super.onResume();

/*		if (!scannedString.equals("") && scannedString != null){

			// TODO Auto-generated method stub
			String data = scannedString;

			Log.e("JP", data);
			Intent resultActivity = new Intent(
					LaserScannerActivity.this, ResultActivity.class);
			resultActivity.putExtra("result", data);
			clipboard.removePrimaryClipChangedListener(listener);
			startActivity(resultActivity);

		}*/


		if (clipboard != null){

			 if (listener== null){
				listener= new OnPrimaryClipChangedListener() {
					
					@Override
					public void onPrimaryClipChanged() {
						// TODO Auto-generated method stub
						String data = clipboard.getPrimaryClip().getItemAt(0).getText().toString();
						
						Log.e("JP", data);
						Intent resultActivity = new Intent(
								LaserScannerActivity.this, ResultActivity.class);
						resultActivity.putExtra("result", data);
						clipboard.removePrimaryClipChangedListener(listener);
						startActivity(resultActivity);
						//finish();
					}
				};
			}
			
			clipboard.addPrimaryClipChangedListener(listener);
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		// respond to menu item selection
		switch (item.getItemId()) {
		case R.id.changeLoc:

			AlertDialog.Builder locBuilder = new AlertDialog.Builder(this);
			locBuilder
					.setMessage(
							"Are you sure you want to change your location settings? You will not be able to scan tickets until an admin approves your location change request.")
					.setCancelable(true)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									startActivity(new Intent(
											LaserScannerActivity.this,
											RequestApprovalActivity.class));
									finish();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog locAlert = locBuilder.create();
			locAlert.show();
			return true;
		case R.id.sync:
			DatabaseHandler dbHandler = new DatabaseHandler(this);
			Integer itemCount = dbHandler.getUnsyncedPassesCount();
			String msg = itemCount.intValue() == 0 ? "No records to sync."
					: (itemCount.intValue() == 1 ? "A record is being synced."
							: itemCount + " records are being synced.");

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
					.setCancelable(true)
					.setNeutralButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// TODO nothing I assume
								}
							});
			AlertDialog alert = builder.create();
			alert.show();

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
