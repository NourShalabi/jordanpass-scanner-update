package com.jordanpass.jordanpass.library;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Dalia on 4/19/2016.
 */
public class ErrorLog {
    private int errorId;
    private int locationId;
    private String errorDate;
    private String extraInfo;

    public ErrorLog(int errorId, int locationId, String extraInfo){
        SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        errorDate = dbDateFormat.format(cal.getTime());

        this.errorId = errorId;
        this.locationId = locationId;
        this.extraInfo = extraInfo;
    }

    public ErrorLog(int errorId, int locationId, String errorDate , String extraInfo){

        this.errorDate = errorDate;
        this.errorId = errorId;
        this.locationId = locationId;
        this.extraInfo = extraInfo;
    }

    public int getErrorId() {
        return errorId;
    }

    public int getLocationId() {
        return locationId;
    }

    public String getErrorDate() {
        return errorDate;
    }

    public String getExtraInfo() {
        return extraInfo;
    }
}
