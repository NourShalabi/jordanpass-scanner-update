package com.jordanpass.jordanpass;



import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jordanpass.jordanpass.library.DatabaseHandler;
import com.jordanpass.jordanpass.library.DeviceId;
import com.jordanpass.jordanpass.library.Error;
import com.jordanpass.jordanpass.library.ErrorLog;
import com.jordanpass.jordanpass.library.Logger;
import com.jordanpass.jordanpass.library.Pass;
import com.jordanpass.jordanpass.library.ResultCheck;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultActivity extends Activity {

    ResultCheck resultChecker;
    String result = "INVALID";

    Logger log;

    Button btn;
    TextView infotxt;
    ImageView validimg;
    ProgressBar spin;
    DeviceId dvIdHandler;
    String deviceId;
    String locId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        btn = (Button) findViewById(R.id.button1);
        infotxt = (TextView) findViewById(R.id.info);
        validimg = (ImageView) findViewById(R.id.imgvalid);
        spin = (ProgressBar) findViewById(R.id.loading);

        btn.setVisibility(View.GONE);
        infotxt.setVisibility(View.GONE);
        validimg.setVisibility(View.GONE);
        spin.setVisibility(View.VISIBLE);

        dvIdHandler = new DeviceId();
        deviceId = dvIdHandler.getDeviceId(getApplicationContext());
        locId = dvIdHandler.getLocationId(getApplicationContext());


        //locId = "1";

        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                 //startActivity(new Intent(ResultActivity.this,
                 //LaserScannerActivity.class));
                 finish();
            }
        });

        log = new Logger("ResultActivity");
        log.e(locId + " --- " + deviceId);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        int locIdInt = 0;
        try {
            locIdInt = Integer.parseInt(locId);
        } catch (NumberFormatException e){

        }

        resultChecker = new ResultCheck(result, locIdInt, this, deviceId) {
            @Override
            public void onError(Error error) {
                String info = error.getEnglishString();
                String info_ar = error.getArabicString();
                btn.setVisibility(View.VISIBLE);
                infotxt.setText(info + "\n\n" + info_ar);
                infotxt.setVisibility(View.VISIBLE);
                validimg.setImageResource(R.drawable.invalid);
                validimg.setVisibility(View.VISIBLE);
                spin.setVisibility(View.GONE);

                addErrorLog(new ErrorLog(error.ordinal()+1, Integer.parseInt(locId), error.getEnglishString()));
            }

            @Override
            public void onError(Error error, String[] extraData) {
                String info = String.format(error.getEnglishString(), extraData);
                String info_ar = String.format(error.getArabicString(), extraData);

                btn.setVisibility(View.VISIBLE);
                infotxt.setText(info + "\n\n" + info_ar);
                infotxt.setVisibility(View.VISIBLE);
                validimg.setImageResource(R.drawable.invalid);
                validimg.setVisibility(View.VISIBLE);
                spin.setVisibility(View.GONE);
                addErrorLog(new ErrorLog(error.ordinal()+1, Integer.parseInt(locId), info));

            }

            @Override
            public void onSuccess(ResultCheck.Result result, boolean offline) {
                btn.setVisibility(View.VISIBLE);
                String info = "Name: " + result.getName() + "\n\n";
                info += "Expiry date: " + result.getDate();
                String info_ar = getResources().getString(
                        R.string.name)
                        + " " + result.getName() + "\n\n";
                info_ar += getResources().getString(
                        R.string.expiry_date)
                        + " " + result.getDate();
                infotxt.setText(info + "\n\n" + info_ar);
                infotxt.setVisibility(View.VISIBLE);
                validimg.setImageResource(R.drawable.valid);
                validimg.setVisibility(View.VISIBLE);
                spin.setVisibility(View.GONE);
            }
        };

        resultChecker.checkResult();
    }

    private void addErrorLog(final ErrorLog errorLog) {
        final DatabaseHandler dbHandler = new DatabaseHandler(ResultActivity.this);
        final RequestQueue queue = Volley.newRequestQueue(ResultActivity.this);
        JsonObjectRequest jsonArrayRequest;

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dbHandler.addErrorLog(errorLog);
            }
        };

        DeviceId dvIdHandler = new DeviceId();
        String deviceId = dvIdHandler.getDeviceId(getApplicationContext());
        String url = "http://jordanpass.jo/scan/Logger.ashx";

        try {
            JSONArray jsonArray = new JSONArray();

            JSONObject json = new JSONObject();
            json.put("deviceId", deviceId);
            json.put("errorId", errorLog.getErrorId());
            json.put("errorDate", errorLog.getErrorDate());
            json.put("locationId", errorLog.getLocationId());
            json.put("extraInfo", errorLog.getExtraInfo());
            jsonArray.put(json);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("logItems", jsonArray);

            Response.Listener<JSONObject> response = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JSONResponse) {

                    try {
                        boolean response = JSONResponse.getBoolean("result");
                        Log.e("LOGERROR", String.valueOf(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dbHandler.addErrorLog(errorLog);
                    }
                }
            };

            jsonArrayRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, response , error);
            queue.add(jsonArrayRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
