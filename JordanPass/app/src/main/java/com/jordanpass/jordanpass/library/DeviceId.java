package com.jordanpass.jordanpass.library;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class DeviceId {

	private Context context;

	public String getDeviceId(Context context) {
		this.context = context;
		SharedPreferences preferences = context.getSharedPreferences(
				Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
		String devId = preferences.getString(Preferences.DEVICE_ID, generateDeviceId());
		return devId;
	};
	
	public String getLocationId(Context context) {
		this.context = context;
		SharedPreferences preferences = context.getSharedPreferences(
				Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
		Integer locId = preferences.getInt(Preferences.LOCATION_ID, 1);
		return locId.toString();
	};

	private String generateDeviceId() {
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();
		return deviceId;
	}
}
