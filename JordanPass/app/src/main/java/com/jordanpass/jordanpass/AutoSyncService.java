package com.jordanpass.jordanpass;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jordanpass.jordanpass.library.DatabaseHandler;
import com.jordanpass.jordanpass.library.DeviceId;
import com.jordanpass.jordanpass.library.ErrorLog;
import com.jordanpass.jordanpass.library.IntentService;
import com.jordanpass.jordanpass.library.Pass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Service started by {@link InternetConnectionReceiver}. Supposed to run when
 * device is connected to the internet. Syncs items in the local DB to main DB,
 * and removes synced items from local DB.
 */

public class AutoSyncService extends IntentService {

    public static Boolean intentRunning = false;
    DatabaseHandler dbHandler;
    List<Pass> passesList;
    StringRequest stringRequest;
    JsonObjectRequest jsonArrayRequest;

    // TODO IMPORTANT! change this when you're done testing.
    String deviceId = "ffffffff-ffff-ffff-ffff-ffffffffffff";
    int backoffTime = 2000;
    int retries = 0;
    boolean started = false;

    public AutoSyncService() {
        super("AutoSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        intentRunning = true; // This intent is running
        dbHandler = new DatabaseHandler(this); // Initiate DB

        // Sleep for 2 seconds while internet connection settles
        // TODO Might not be the best idea. Change this?
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Get Device Id
        final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(),
                ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        deviceId = deviceUuid.toString();

        // Get unsynced passes, store in passesList
        passesList = new ArrayList<Pass>();
        dbHandler.deleteExpiredPasses();
        if ((!started) && (dbHandler.getUnsyncedErrorLogsCount() > 0)) syncLogs();
        if (dbHandler.getUnsyncedPassesCount() > 0) {
            passesList.addAll(dbHandler.getUnsyncedPasses());
            SyncItem(passesList.get(0)); // Sync the first item

        } else {
            // Remove all instances of this service that are scheduled to run.
            clearQueue();
        }

    }

    @Override
    public void onStart(Intent intent, int startId) {
        clearQueue();
        super.onStart(intent, startId);
        // Log.e("service", "I STARTED!!!!");
    }

    @Override
    public void onDestroy() {
        intentRunning = false;
        super.onDestroy();
    }

    /**
     * Syncs tickets to main DB
     *
     * @param pass The ticket to be synced
     */
    public void SyncItem(final Pass pass) {
        backoffTime = 5000;
        retries = 0;

        final RequestQueue queue = Volley.newRequestQueue(AutoSyncService.this);
        Response.ErrorListener error = new Response.ErrorListener() {

            /*
             * DONE: If pass fails to sync, back off and try again in
             * backoffTime milliseconds. If it still fails after 4 retries leave
             * it and sync next item.
             */
            @Override
            public void onErrorResponse(VolleyError error) {
                if (retries < 4) {

                    try {
                        Thread.sleep(backoffTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.e("jordanPass", e.getMessage());
                    }

                    backoffTime *= 2;
                    retries++;
                    queue.add(stringRequest);

                } else {
                    backoffTime = 5000;
                    retries = 0;
                    SyncNextItem();
                }
            }
        };

        Response.Listener<String> response = new Response.Listener<String>() {

			/*
             * DONE: parse the response, see if it succeeded. if it did, remove
			 * item from DB, if it didn't, skip to the next item anyway and
			 * decide what to do with the item that failed later (TODO). Reset
			 * backoffTime and sync next item.
			 */

            @Override
            public void onResponse(String response) {
                // TODO if the response is null or empty, retry.

                // If pass synced successfully
                if (response.equalsIgnoreCase("true")
                        || response.equalsIgnoreCase("false")) {
                    // remove pass from local DB, reset backoffTime and number
                    // of retries
                    dbHandler.deletePass(pass);
                    backoffTime = 5000;
                    retries = 0;
                }

                SyncNextItem();
            }
        };

        DeviceId dvIdHandler = new DeviceId();
        String deviceId = dvIdHandler.getDeviceId(getApplicationContext());
        String locId = dvIdHandler.getLocationId(getApplicationContext());
        String url = String
                .format("http://jordanpass.jo/scan/OfflineScan.ashx?PassId=%s&PassTypeId=%s&LocationId=%s&DeviceCode=%s&PassLocationDate=%s",
                        ((Integer) pass.GetPassId()).toString(),
                        ((Integer) pass.GetPassType()).toString(), locId,
                        deviceId,
                        Uri.encode(pass.GetScanTime() + ".000"));

        stringRequest = new StringRequest(Request.Method.GET, url, response,
                error) {

            // TODO use this if Request Method changes to POST
			/*
			 * @Override protected Map<String, String> getParams() throws
			 * com.android.volley.AuthFailureError { Map<String, String> params
			 * = new HashMap<String, String>(); params.put("passId", "" +
			 * pass.GetPassId()); params.put("locationId", "" +
			 * pass.GetLocationId()); params.put("scanTime",
			 * pass.GetScanTime()); params.put("deviceId", deviceId); return
			 * params; };
			 */
        };

        queue.add(stringRequest);

    }

    /**
     * Remove first item from passList until it's empty. Sync next item while
     * it's not.
     */
    protected void SyncNextItem() {
        passesList.remove(0);
        if (passesList.size() > 0) {
            SyncItem(passesList.get(0));
        } else {
            // Remove all instances of this service that are scheduled to run
            // after this one.
            clearQueue();
            //syncLogs();
        }

    }

    private void syncLogs() {
        backoffTime = 5000;
        retries = 0;
        started = true;

        final RequestQueue queue = Volley.newRequestQueue(AutoSyncService.this);


        Response.ErrorListener error = new Response.ErrorListener() {

            /*
             * DONE: If pass fails to sync, back off and try again in
             * backoffTime milliseconds. If it still fails after 4 retries leave
             * it and sync next item.
             */
            @Override
            public void onErrorResponse(VolleyError error) {
                /*if (retries < 4) {

                    try {
                        Thread.sleep(backoffTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.e("LOGERROR", e.getMessage());
                    }

                    backoffTime *= 2;
                    retries++;
                    queue.add(jsonArrayRequest);

                } else {
                    started = false;
                }*/
                started = false;
            }
        };



        // TODO IMPORTANT!! Remove the hardcoded deviceId
        DeviceId dvIdHandler = new DeviceId();
        String deviceId = dvIdHandler.getDeviceId(getApplicationContext());
        String url = "http://jordanpass.jo/scan/Logger.ashx";

        List<ErrorLog> errorLogs = dbHandler.getUnsyncedErrorLogs();
        try {
            JSONArray jsonArray = new JSONArray();
            for (ErrorLog errorLog : errorLogs) {
                JSONObject json = new JSONObject();
                json.put("deviceId", deviceId);
                json.put("errorId", errorLog.getErrorId());
                json.put("errorDate", errorLog.getErrorDate());
                json.put("locationId", errorLog.getLocationId());
                json.put("extraInfo", errorLog.getExtraInfo());

                jsonArray.put(json);
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("logItems", jsonArray);

            Response.Listener<JSONObject> response = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JSONResponse) {

                    try {
                        boolean response = JSONResponse.getBoolean("result");
                        // remove pass from local DB, reset backoffTime and number
                        // of retries
                        Log.e("LOGERROR", String.valueOf(response));
                        dbHandler.deleteAllUnsyncedErrorLogs();
                        backoffTime = 5000;
                        retries = 0;
                        started = false;

                    } catch (JSONException e) {
                        e.printStackTrace();
                        started = false;
                    }



                }
            };


            jsonArrayRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, response , error);

            queue.add(jsonArrayRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }



		/*stringRequest = new StringRequest(Request.Method.POST, url, response,
				error);*/


    }
}
