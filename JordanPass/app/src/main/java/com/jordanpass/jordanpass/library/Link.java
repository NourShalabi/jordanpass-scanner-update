package com.jordanpass.jordanpass.library;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Requests permission to use the app with this device.
 * 
 * @author Dalia
 * 
 */
public class Link {

	String deviceId = "";
	public static final Integer REQUEST_PERMISSION = 0;
	Logger log;
	Context context;
	String gcmRegId = "";

	/**
	 * Requests permission to use the app with this device.
	 * 
	 * @param context
	 */
	public Link(Context context) {

		this.context = context;
		log = new Logger("Link");
		final TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		deviceId = deviceUuid.toString();

		SharedPreferences preferences = context.getSharedPreferences(
				Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
		gcmRegId = preferences.getString(Preferences.GCM_REG_ID, "");

	}

	// -------------------------------------------------------------------------------------------------------------

	/**
	 * Requests permission to use this app on this specific device. Approval
	 * comes as a push message.
	 * 
	 * @param name
	 * @param email
	 * @param location
	 *            Location this app will be used from
	 */
	public void RequestPermission(String name, String email, String mobile,
			Integer location) {

		// new RequestPermissionAsync().execute(name, email,
		// location.toString());
		SharedPreferences preferences = context.getSharedPreferences(
				Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = preferences.edit();
		edit.putInt("Location", location);
		edit.commit();

		RequestPermissionVolley(name, email, mobile, location.toString());

	}

	public void RequestPermissionResult(Boolean success) {
	}

	private class RequestPermissionAsync extends
			AsyncTask<String, Void, Boolean> {

		Boolean success = true;

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO
			/*
			 * Request permission, return success
			 */
			log.e("Requseting Permission");
			return success;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (success) {
				SharedPreferences preferences = context.getSharedPreferences(
						Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
				SharedPreferences.Editor edit = preferences.edit();
				edit.putInt("ApprovalStatus", ApprovalStatus.PENDING);
				edit.commit();
			}

			RequestPermissionResult(result);
		}
	}

	public void RequestPermissionVolley(final String name, final String email,
			final String mobile, final String location) {

		Response.ErrorListener error = new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {

				if (error instanceof NoConnectionError) {
					log.e("No internet access. Please check your internet connection.");
				}

				RequestPermissionResult(false);
			}
		};

		RequestQueue queue = Volley.newRequestQueue(context);
		// String url = "http://validate.jsontest.com/";
		String url = String
				.format("AddDevice.ashx?DeviceCode=%s&RegistrationId=%s&DeviceName=%s&DeviceEmail=%s&DeviceMobile=%s&LocationId=%s",
						deviceId, gcmRegId, name, email, mobile, location);

		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {

						// TODO

						/*
						 * parse response to JSON, according decide if it was a
						 * success or failure
						 */

						/*
						 * SharedPreferences preferences = context
						 * .getSharedPreferences(Preferences.APP_SETTINGS,
						 * Context.MODE_PRIVATE); SharedPreferences.Editor edit
						 * = preferences.edit(); edit.putInt("ApprovalStatus",
						 * ApprovalStatus.PENDING); edit.commit();
						 * 
						 * RequestPermissionResult(true);
						 */
					}
				}, error) {

			/*
			 * @Override protected Map<String, String> getParams() throws
			 * com.android.volley.AuthFailureError { Map<String, String> params
			 * = new HashMap<String, String>(); params.put("name", name);
			 * params.put("email", email); params.put("location", location);
			 * params.put("deviceId", deviceId); params.put("gcmRegId",
			 * gcmRegId);
			 * 
			 * return params; };
			 */

		};

		stringRequest.setRetryPolicy(new DefaultRetryPolicy(
				DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		// TODO
		// queue.add(stringRequest);
	}

	// -------------------------------------------------------------------------------------------------------------

	/**
	 * Check the validity of a ticket.
	 * 
	 * @param pass
	 *            the encrypted string extracted from the QRcode
	 */

	public void CheckValidity(String pass) {
		if (CheckFormat(pass)) {
			SharedPreferences preferences = context.getSharedPreferences(
					Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
			String location = preferences.getInt("Location", 0) + "";

			CheckValidityVolley(pass, GetPassType(pass), location);
			// new CheckValidityAsync().execute(pass);
		} else
			CheckValidityResult(false, false);
	}

	private Boolean CheckFormat(String pass) {
		Boolean result = false;
		// TODO Decrypt pass
		// TODO check if decrypted pass follows the format
		return result;
	}

	private String GetPassType(String pass) {

		return "";
		// TODO return passType;
	}

	public void CheckValidityResult(Boolean success, Boolean valid) {
	}

	private class CheckValidityAsync extends AsyncTask<String, Void, Boolean> {

		Boolean success = true;
		Boolean validity = false;

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO
			/*
			 * is request successful? is pass valid?
			 */
			log.e("Checking ticket validity");
			return success;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			if (!success)
				validity = true;
			CheckValidityResult(success, validity);
		}
	}

	public void CheckValidityVolley(final String passId, final String passType,
			final String location) {

		Response.ErrorListener error = new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {

				if (error instanceof NoConnectionError) {
					log.e("No internet access. Please check your internet connection.");
				}

				CheckValidityResult(false, true);
			}
		};

		RequestQueue queue = Volley.newRequestQueue(context);
		// TODO
		String url = String
				.format("OnlineScan.ashx?PassId=%s&PassTypeId=%s&LocationId=%s&DeviceCode=%s",
						passId, passType, location, deviceId);

		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Boolean validity = false;
						// TODO check validity from response
						CheckValidityResult(true, validity);
					}
				}, error) {

			/*
			 * @Override protected Map<String, String> getParams() throws
			 * com.android.volley.AuthFailureError { Map<String, String> params
			 * = new HashMap<String, String>(); params.put("passId", passId);
			 * return params; };
			 */

		};

		stringRequest.setRetryPolicy(new DefaultRetryPolicy(
				DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		// TODO
		// queue.add(stringRequest);
	}

}
