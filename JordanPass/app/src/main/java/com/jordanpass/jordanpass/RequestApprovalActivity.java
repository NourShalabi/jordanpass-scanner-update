package com.jordanpass.jordanpass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jordanpass.jordanpass.library.ApprovalStatus;
import com.jordanpass.jordanpass.library.Link;
import com.jordanpass.jordanpass.library.Logger;
import com.jordanpass.jordanpass.library.Preferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RequestApprovalActivity extends Activity {

	EditText tb_name, tb_email, tb_mobile;
	Spinner sp_location;
	Button bt_submit;
	PermissionRequester link;
	Logger log;
	List<String> locList;
	List<Integer> locIdList;
	ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_request_approval);

		locList = new ArrayList<String>();
		locIdList = new ArrayList<Integer>();

		// Init views
		tb_name = (EditText) findViewById(R.id.editText1);
		tb_email = (EditText) findViewById(R.id.editText2);
		tb_mobile = (EditText) findViewById(R.id.editText3);
		sp_location = (Spinner) findViewById(R.id.spinner1);
		bt_submit = (Button) findViewById(R.id.button1);

		// DONE: dialog that waits until location
		// list is retrieved from DB
		progress = ProgressDialog.show(this, "Fetching Locations",
				"Please wait...", true, false);

		Response.ErrorListener error = new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {

				if (error instanceof NoConnectionError) {
					log.e("No internet access. Please check your internet connection.");
				}
				progress.dismiss();

				// DONE: Dialog/Toast - couldn't connect to internet/fetch
				// locations.
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RequestApprovalActivity.this);
				builder.setMessage(
						"Failed to fetch locations. Please check your internet connectivity.")
						.setCancelable(false)
						.setNeutralButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										finish();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();

			}
		};

		// Fetch locations
		RequestQueue queue = Volley.newRequestQueue(this);
		String url = "http://jordanpass.jo/scan/locations.ashx";
		StringRequest locationRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						// Faulty JSON fixes
						response = response.replace("\n", "");
						response = response.replace("\r", "");
						response = response.replace(",}", "}");
						response = response.replace(",]", "]");

						try {
							JSONObject json = new JSONObject(response);
							JSONArray locJsonList = json.getJSONArray("item");
							for (int i = 0; i < locJsonList.length(); i++) {
								locList.add(locJsonList.getJSONObject(i)
										.getString("Name"));
								locIdList.add(Integer.parseInt(locJsonList
										.getJSONObject(i).getString("id")));
							}

						} catch (JSONException e) {

							// DONE: Dialog/Toast - couldn't connect to
							// internet/fetch locations (Or faulty JSON is even
							// more so)
							AlertDialog.Builder builder = new AlertDialog.Builder(
									RequestApprovalActivity.this);
							builder.setMessage(
									"Failed to fetch locations. Please check your internet connectivity.")
									.setCancelable(false)
									.setNeutralButton(
											"OK",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													finish();
												}
											});
							AlertDialog alert = builder.create();
							alert.show();
							e.printStackTrace();
						}

						// DONE Populate spinner with fetched locations
						ArrayAdapter<String> locAdapter = new ArrayAdapter<String>(
								RequestApprovalActivity.this,
								android.R.layout.simple_spinner_item, locList);

						locAdapter
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp_location.setAdapter(locAdapter);

						// DONE: dismiss dialog
						progress.dismiss();
					}
				}, error) {

		};
		queue.add(locationRequest);

		link = new PermissionRequester();
		log = new Logger("RequestApproval");

		// Submit stuff on button click
		bt_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String name = tb_name.getText().toString();
				String email = tb_email.getText().toString();
				String mobile = tb_mobile.getText().toString();
				Integer location = locIdList.get(sp_location
						.getSelectedItemPosition());

				// Don't submit empty fields
				if (name.isEmpty() || email.isEmpty() || mobile.isEmpty()) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							RequestApprovalActivity.this);
					builder.setMessage("Please make sure to fill all fields.")
							.setCancelable(false)
							.setNeutralButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// TODO nothing I assume
										}
									});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					// If no empty fields, submit request
					link.RequestPermission(name, email, mobile, location);
				}
			}
		});

	}

	

	private class PermissionRequester {

		public void RequestPermission(String name, String email, String mobile,
				final Integer locationId) {

			progress = ProgressDialog.show(RequestApprovalActivity.this,
					"Submitting Request", "Please wait...", true, false);

			Context context = RequestApprovalActivity.this;
			Response.ErrorListener error = new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					if (error instanceof NoConnectionError) {
						log.e("No internet access. Please check your internet connection.");
						progress.dismiss();

						// DONE: toast/prompt things failed
						AlertDialog.Builder builder = new AlertDialog.Builder(
								RequestApprovalActivity.this);
						builder.setMessage(
								"Unable to connect to server. Please check your internet connectivity.")
								.setCancelable(false)
								.setNeutralButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// TODO do I need to do anything
												// here?
											}
										});
						AlertDialog alert = builder.create();
						alert.show();
					}

				}
			};

			SharedPreferences preferences = context.getSharedPreferences(
					Preferences.APP_SETTINGS, Context.MODE_PRIVATE);
			String gcmRegId = preferences.getString(Preferences.GCM_REG_ID, "");
			String deviceId = preferences.getString(Preferences.DEVICE_ID, "");
			if (deviceId.equals("")) {
				final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

				final String tmDevice, tmSerial, androidId;
				tmDevice = "" + tm.getDeviceId();
				tmSerial = "" + tm.getSimSerialNumber();
				androidId = ""
						+ android.provider.Settings.Secure.getString(
								getContentResolver(),
								android.provider.Settings.Secure.ANDROID_ID);

				UUID deviceUuid = new UUID(androidId.hashCode(),
						((long) tmDevice.hashCode() << 32)
								| tmSerial.hashCode());
				deviceId = deviceUuid.toString();

			}

			RequestQueue queue = Volley.newRequestQueue(context);

			String url = String
					.format("http://jordanpass.jo/scan/AddDevice.ashx?DeviceCode=%s&RegistrationId=%s&DeviceName=%s&DeviceEmail=%s&DeviceMobile=%s&LocationId=%s",
							Uri.encode(deviceId), Uri.encode(gcmRegId),
							Uri.encode(name), Uri.encode(email),
							Uri.encode(mobile), locationId);

			StringRequest stringRequest = new StringRequest(Request.Method.GET,
					url, new Response.Listener<String>() {

						@Override
						public void onResponse(String response) {

							// TODO check if true, false, or something else
							if (response.equalsIgnoreCase("true")) {
								SharedPreferences preferences = getSharedPreferences(
										Preferences.APP_SETTINGS, MODE_PRIVATE);
								SharedPreferences.Editor edit = preferences
										.edit();

								edit.putInt(Preferences.APPROVAL_STATUS,
										ApprovalStatus.PENDING);

								edit.putInt(Preferences.LOCATION_ID, locationId);

								edit.commit();
								progress.dismiss();
								startActivity(new Intent(
										RequestApprovalActivity.this,
										ApprovalPendingActivity.class));
								finish();
							} else {
								// DONE:
								// Toast/Prompt that things failed
								progress.dismiss();
								AlertDialog.Builder builder = new AlertDialog.Builder(
										RequestApprovalActivity.this);
								builder.setMessage(
										"Request failed. Try again later or contact support.")
										.setCancelable(false)
										.setNeutralButton(
												"OK",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// TODO do I need to do
														// anything here?
													}
												});
								AlertDialog alert = builder.create();
								alert.show();
							}

						}
					}, error) {

			};

			stringRequest.setRetryPolicy(new DefaultRetryPolicy(
					DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			if (gcmRegId.equals("")) {
				// DONE: toast that device isn't registered with GCM and
				// restarting the app might fix the problem
				// or PROMPT
				progress.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RequestApprovalActivity.this);
				builder.setMessage(
						"Device is not registered. Make sure that this device is connected to a Google account. If so, try restarting the app.")
						.setCancelable(false)
						.setNeutralButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// TODO do I need to do anything here?
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			} else
				queue.add(stringRequest);

		}

	}

}
