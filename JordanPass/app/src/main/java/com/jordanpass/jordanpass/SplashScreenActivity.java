package com.jordanpass.jordanpass;

import java.util.UUID;

import com.google.android.gcm.GCMRegistrar;
import com.jordanpass.jordanpass.library.ApprovalStatus;
import com.jordanpass.jordanpass.library.Preferences;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Splash Screen Activity. Initializes app and decides where to go next.
 */

public class SplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// App preferences
		SharedPreferences preferences = getSharedPreferences(
				Preferences.APP_SETTINGS, MODE_PRIVATE);
		SharedPreferences.Editor edit = preferences.edit();

		// Initializes app on first run. Saves DeviceId and sets ApprovalStatus
		// to Not Approved.
		Boolean isFirstRun = preferences
				.getBoolean(Preferences.FIRST_RUN, true);
		if (isFirstRun) {

			// TODO just turn this to some function or so since you're using it
			// too much
			// ------------------DeviceId--------------------
			final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			final String tmDevice, tmSerial, androidId;
			tmDevice = "" + tm.getDeviceId();
			tmSerial = "" + tm.getSimSerialNumber();
			androidId = ""
					+ android.provider.Settings.Secure.getString(
							getContentResolver(),
							android.provider.Settings.Secure.ANDROID_ID);

			UUID deviceUuid = new UUID(androidId.hashCode(),
					((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
			String deviceId = deviceUuid.toString(); // Unique ID
			// ----------------------------------------------

			edit.putString(Preferences.DEVICE_ID, deviceId);
			edit.putInt(Preferences.APPROVAL_STATUS,
					ApprovalStatus.NOT_APPROVED);
			edit.putBoolean(Preferences.FIRST_RUN, false);
			edit.commit(); // commit changes to app preferences
		}
		registerDevice(); // Register device to GCM

		int approvalStatus = preferences.getInt(Preferences.APPROVAL_STATUS,
				ApprovalStatus.NOT_APPROVED); // Default status is not approved

		// TODO remove this when you're done testing
		// approvalStatus = 0;
		// startActivity(new Intent(this, MainScannerActivity.class));
		// finish();
		approvalStatus =  ApprovalStatus.APPROVED;

		switch (approvalStatus) {
		case ApprovalStatus.NOT_APPROVED:
			// Not Approved: go to approval screen
			startActivity(new Intent(this, RequestApprovalActivity.class));
			finish();
			break;
		case ApprovalStatus.APPROVED:
			// Approved: go to scanner screen
			startActivity(new Intent(this, MainScannerActivity.class));
			startActivity(new Intent(this, LaserScannerActivity.class));
			finish();
			break;
		case ApprovalStatus.PENDING:
			// Pending approval: pending approval screen?
			startActivity(new Intent(this, ApprovalPendingActivity.class));
			finish();
			break;
		}

	}

	/**
	 * Checks if device is registered to GCM. If not, attempts to register it.
	 */
	public void registerDevice() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) // If device is not registered, register
			GCMRegistrar.register(getApplicationContext(), Preferences.GCM_KEY);

		else
			Log.i("regid", regId);
	}
}
