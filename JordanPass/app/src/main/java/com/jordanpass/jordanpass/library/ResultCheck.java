package com.jordanpass.jordanpass.library;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Check the validity of the QRCode result.
 * Created by Dalia on 4/12/2016.
 */
public abstract class ResultCheck {

    private final static int NO_CONNECTION = 0;
    private final static int TICKET_VALID = 1;
    private final static int TICKET_INVALID = 2;
    private final static int TICKET_EXPIRED = 3;
    private final static int TICKET_USED = 4;
    DatabaseHandler db;
    Context context;
    SimpleDateFormat dbDateFormat;
    private String rawResult;
    private String decryptedResult;
    private int locationId;
    private String deviceId;

    /**
     * @param result     String raw output from camera/scanner.
     * @param locationId Current location ID.
     * @param context
     * @param deviceId
     */
    public ResultCheck(String result, int locationId, Context context, String deviceId) {
        rawResult = result;
        this.locationId = locationId;
        this.context = context;
        db = new DatabaseHandler(context);
        dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.deviceId = deviceId;
    }


    /**
     * Checks if the ticket is valid or not. Fires onSuccess and onError accordingly.
     */
    public void checkResult() {
        Result result;
        int onlineCheckResult = 0;
        try {
            decryptedResult = decrypt(rawResult);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | InvalidKeyException | UnsupportedEncodingException | BadPaddingException | InvalidKeySpecException e) {
            onError(Error.ENCRYPTION_ERROR);
        }
        result = checkOffline(decryptedResult);
        if (result != null) {
            checkOnline(result);

        }


    }

    private void addTicketToDB(Result result, boolean offline) {
        Calendar cal = Calendar.getInstance();
        if (offline) {
            db.addPass(new Pass(result.getTicketId(), result.getTicketType(), result.getDate(),
                    dbDateFormat.format(cal.getTime())));
        }

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date dateObj = formatter.parse(result.getDate());
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            db.addPassToAllPasses(result.getTicketId(), result.getTicketType(), dateObj.getTime(),
                    dbDateFormat.format(cal.getTime()));
        } catch (ParseException e) {
            db.addPassToAllPasses(result.getTicketId(), result.getTicketType(), cal.getTimeInMillis(),
                    dbDateFormat.format(cal.getTime()));
        }


    }

    private void noConnection(Result result){
        boolean validLocal = checkLocalDatabase(result);
        if (validLocal) {
            onSuccess(result, true);
            addTicketToDB(result, true);
        }
    }

    private void ticketValid(Result result){
        onSuccess(result, false);
        addTicketToDB(result, false);
    }

    private void checkOnline(final Result ticket) {
        final int[] checkResult = new int[1];

        //region errorListener
        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError) {
                    Log.e("result check", "No internet access. Please check your internet connection.");
                }
                checkResult[0] = NO_CONNECTION;
                noConnection(ticket);
            }
        };
        //endregion
        //region responseListener
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("\n", "");
                response = response.replace("\r", "");
                response = response.replace(",}", "}");
                String dateTemp = "";
                checkResult[0] = TICKET_VALID;


                try {
                    JSONObject json = new JSONObject(response);
                    String result = json.getString("PassExists");
                    if (result.equalsIgnoreCase("True")) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        Calendar calendar = Calendar.getInstance();
                        Boolean active = false;

                        //region check ticket expiry date
                        if (json.getString("ActivationDate")
                                .equalsIgnoreCase("null")) {
                            dateTemp = json.getString("ExpiryDate");
                            calendar = Calendar.getInstance();
                            Date expiryDate = formatter.parse(dateTemp);

                            if (expiryDate.before(calendar.getTime())) {
                                checkResult[0] = TICKET_EXPIRED;
                                onError(Error.TICKET_EXPIRED_YEAR, new String[]{dateTemp});
                            } else {
                                checkResult[0] = TICKET_VALID;
                                SimpleDateFormat tempFormat = new SimpleDateFormat("MM/dd/yyyy");
                                ticket.setDate(tempFormat.format(expiryDate));
                            }
                            active = !(expiryDate.before(calendar
                                    .getTime()));
                        } else {
                            Date activationDate = formatter.parse(json
                                    .getString("ActivationDate"));
                            calendar.add(Calendar.DATE, -14);
                            active = !(activationDate.before(calendar
                                    .getTime()));

                            calendar.setTimeInMillis(activationDate.getTime());
                            calendar.add(Calendar.DATE, 14);
                            formatter = new SimpleDateFormat(
                                    "MM/dd/yyyy");
                            dateTemp = formatter.format(calendar
                                    .getTime());

                            if (active) {
                                checkResult[0] = TICKET_VALID;
                                ticket.setDate(dateTemp);
                            } else {
                                checkResult[0] = TICKET_EXPIRED;
                                onError(Error.TICKET_EXPIRED_SINCE_ACTIVATION, new String[]{dateTemp});
                            }
                        }

                        //endregion

                        //region check number of scans for location
                        if (checkResult[0] == TICKET_VALID) {
                            int numberOfVisits = Integer.parseInt(json.getString("VistNu"));
                            int allowedNumberOfVisits = allowedNumberOfVisits(ticket);
                            if (numberOfVisits >= allowedNumberOfVisits) {
                                //region number of visits exceeds allowed
                                if (json.has("ScanDate")) {
                                    String strScanDate = json.getString("ScanDate");
                                    String[] arrScanDate = strScanDate.split(" ");
                                    strScanDate = arrScanDate[0] + " ";
                                    strScanDate += arrScanDate[1];
                                    strScanDate += " ";
                                    strScanDate += (arrScanDate[2].equals("ص")? "AM" : "PM");
                                    SimpleDateFormat scanDateFormatter = new SimpleDateFormat(
                                            "dd/MM/yyyy hh:mm:ss a");
                                    Date scanDate = scanDateFormatter.parse(strScanDate);
                                    if (!tenSecondsCheck(scanDate)) {
                                        checkResult[0] = TICKET_USED;
                                        onError(Error.TICKET_ALREADY_USED, new String[]{strScanDate});
                                    }
                                } else {
                                    checkResult[0] = TICKET_INVALID;
                                    onError(Error.OTHER);
                                }
                                //endregion
                            } else {
                                if (locationId == Location.PETRA) {
                                    //region check if ticket scanned today
                                    if (json.has("ScanDate")) {
                                        String strScanDate = json.getString("ScanDate");
                                        String[] arrScanDate = strScanDate.split(" ");
                                        strScanDate = arrScanDate[0] + " ";
                                        strScanDate += arrScanDate[1];
                                        strScanDate += " ";
                                        strScanDate += (arrScanDate[2].equals("ص") ? "AM" : "PM");
                                        SimpleDateFormat scanDateFormatter = new SimpleDateFormat(
                                                "dd/MM/yyyy hh:mm:ss a");
                                        Date scanDate = scanDateFormatter.parse(strScanDate);
                                        Calendar todayOrModified = Calendar.getInstance();
                                        Calendar calScanDate = Calendar.getInstance();
                                        calScanDate.setTime(scanDate);
                                        resetCalendarTime(todayOrModified);
                                        resetCalendarTime(calScanDate);
                                        if (todayOrModified.compareTo(calScanDate) == 0) {
                                            if (!tenSecondsCheck(scanDate)) {
                                                checkResult[0] = TICKET_USED;
                                                onError(Error.TICKET_ALREADY_USED_TODAY, new String[]{strScanDate});
                                            }
                                        }
                                    } else if(numberOfVisits != 0){
                                        checkResult[0] = TICKET_INVALID;
                                        onError(Error.OTHER);
                                    }
                                    //endregion
                                }
                            }
                            //endregion

                        }
                    } else {
                        checkResult[0] = TICKET_INVALID;
                        onError(Error.INVALID_TICKET);
                    }
                } catch (JSONException | ParseException e) {
                    checkResult[0] = NO_CONNECTION;
                    noConnection(ticket);
                }

                if (checkResult[0] == TICKET_VALID){
                    ticketValid(ticket);
                }
            }
        };
        //endregion

        RequestQueue queue = Volley.newRequestQueue(context);

        String url = String
                .format("http://jordanpass.jo/scan/OnlineScan.ashx?PassId=%d&PassTypeId=%d&LocationId=%d&DeviceCode=%s",
                        ticket.getTicketId(), ticket.getTicketType(), locationId, deviceId);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, response, error);


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);

    }


    /**
     * Checks if ticket format is valid and haven't been scanned previously on this device.
     *
     * @param decryptedResult Ticket code after decryption.
     * @return The details of the ticket if valid, null if invalid.
     */
    private Result checkOffline(String decryptedResult) {
        Result result;
        result = ticketSplitter(decryptedResult);
        if (result == null) {
            return null;
        }
        //If this is the baptism site, check if ticket allows entry
        if (locationId == Location.BAPTISM) {
            if (!result.getBaptismEntry()) {
                onError(Error.NO_LOCATION_ACCESS);
                return null;
            }
        }

        return result;
    }

    private boolean checkLocalDatabase(Result result) {
        //if ticket has been scanned on this device before
        if (db.getPassByIdFromAllPasses(result.getTicketId()) != null) {
            int allowedNumberOfVisits = allowedNumberOfVisits(result);

            try {
                //region check for number of scans
                //if this ticket has less scans than allowed number of entries
                if (db.getPassByIdFromAllPasses(result.getTicketId())
                        .GetNumberOfScans() < allowedNumberOfVisits) {
                    //region ticket withing limit of allowed number of scans
                    if ((allowedNumberOfVisits > 0) && (db.getPassByIdFromAllPasses(result.getTicketId()).GetNumberOfScans() == 0)) {
                        return true;
                    } else {
                        Calendar todayOrModified = Calendar.getInstance();

                        Date lastScan = dbDateFormat.parse(db.getPassByIdFromAllPasses(result.getTicketId()).GetScanTime());
                        Date firstScan = dbDateFormat.parse(db.getPassByIdFromAllPasses(result.getTicketId()).GetFirstScanTime());

                        //allowedNumberOfVisits are days in a row from first scan
                        //hence the check if today is in range of allowed days from first scan
                        todayOrModified.add(Calendar.DATE, -(allowedNumberOfVisits - 1));
                        resetCalendarTime(todayOrModified);
                        if (firstScan.compareTo(todayOrModified.getTime()) >= 0) {
                            //region ticket within useable days for this location
                            todayOrModified = Calendar.getInstance();
                            resetCalendarTime(todayOrModified);

                            Calendar scanCal = Calendar.getInstance();
                            scanCal.setTime(lastScan);
                            resetCalendarTime(scanCal);

                            //now check if the ticket was already used today, you get one use per day
                            if (todayOrModified.compareTo(scanCal) == 0) {
                                //region ticket was used today
                                //was the scan a few seconds ago?
                                if (tenSecondsCheck(lastScan)) {
                                    return true;
                                } else {
                                    onError(Error.TICKET_ALREADY_USED_TODAY, new String[]{db.getPassByIdFromAllPasses(result.getTicketId()).GetScanTime()});
                                    return false;
                                }
                                //endregion
                            } else {
                                //region ticket wasn't used today
                                return true;
                                //endregion
                            }
                            //endregion
                        } else {
                            //region ticken no longer valid for this region
                            if (tenSecondsCheck(lastScan)) {
                                return true;
                            } else {
                                onError(Error.TICKET_EXPIRED_FOR_LOCATION, new String[]{db.getPassByIdFromAllPasses(result.getTicketId()).GetFirstScanTime()});
                                return false;
                            }
                            //endregion
                        }
                    }
                    //endregion
                } else {
                    //region ticket exceeded the number of allowed scans
                    Date lastScan = dbDateFormat.parse(db.getPassByIdFromAllPasses(result.getTicketId()).GetScanTime());
                    if (tenSecondsCheck(lastScan)) {
                        return true;
                    } else {
                        onError(Error.TICKET_ALREADY_USED, new String[]{db.getPassByIdFromAllPasses(result.getTicketId()).GetScanTime()});
                        return false;
                    }
                    //endregion
                }

                //endregion
            } catch (ParseException e) {
                onError(Error.OTHER);
                return false;
            }
        } else {
            //ticket hasn't been scanned on this device before
            return true;
        }
    }


    /**
     * Checks if a ticket was scanned within the last 10 seconds.
     *
     * @param lastScan Last time this ticket was scanned.
     * @return true if ticket was scanned withen 10 seconds or less, false otherwise.
     */
    private boolean tenSecondsCheck(Date lastScan) {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.SECOND, -10);
        Calendar scanCal = Calendar.getInstance();
        scanCal.setTime(lastScan);

        return scanCal.compareTo(today) >= 0;
    }


    /**
     * Resets the time part of the calendar to 00:00:00.000.
     *
     * @param calendar Calendar to be reset.
     */
    private void resetCalendarTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }


    /**
     * @param result The ticket to check.
     * @return Number of visits allowed.
     */
    private int allowedNumberOfVisits(Result result) {
        if (result == null) {
            onError(Error.OTHER);
            return 0;
        }

        int allowedNumberOfVisits = 1;
        if (locationId == Location.PETRA) {
            allowedNumberOfVisits = result.ticketType;
        }
        return allowedNumberOfVisits;
    }


    /**
     * Splits tickets into chunks and parses them.
     *
     * @param decryptedResult Ticket code after decryption.
     * @return The details of the ticket if valid, null if invalid.
     */
    private Result ticketSplitter(String decryptedResult) {
        Result result = new Result();
        Log.e("test", decryptedResult);

        //Split ticket to chunks
        String[] passArray = decryptedResult.split("_");
        if (passArray.length < 4) {
            onError(Error.INVALID_TICKET_FORMAT);
            return null;
        } else {
            //region Parse ticket ID
            try {
                result.setTicketId(Integer.parseInt(passArray[0]));
            } catch (Exception e) {
                onError(Error.INVALID_TICKET_FIELD);
                return null;
            }
            //endregion

            //region Parse ticket type
            try {
                result.setTicketType(Integer.parseInt(passArray[1]));
            } catch (Exception e) {
                onError(Error.INVALID_TICKET_FIELD);
                return null;
            }
            //endregion

            //region Parse ticket expiry date
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(
                        "MM/dd/yyyy");
                Calendar calendar = Calendar.getInstance();
                passArray[2] = passArray[2].replace(".", "/").replace("-", "/");
                Date expiryDate = formatter.parse(passArray[2]);

                //If expiry date is before today, this ticket has expired
                if ((expiryDate.before(calendar.getTime()))) {
                    onError(Error.TICKET_EXPIRED_YEAR, new String[]{passArray[2]});
                    return null;
                } else {
                    result.setDate(passArray[2]);
                }

            } catch (Exception e) {
                onError(Error.INVALID_TICKET_FIELD);
                return null;
            }
            //endregion

            //region Parse Baptism Site availability
            String lastChunk = passArray[passArray.length - 1];
            String[] extras = lastChunk.split(":");
            result.setBaptismEntry(false);
            if (extras.length >= 2) {
                for (int i = 1; i < extras.length; i++) {
                    if (extras[i].equals("022")) {
                        result.setBaptismEntry(true);
                        break;
                    }
                }
            }
            //endregion

            //region Parse Name
            String name = "";
            for (int i = 3; i < passArray.length; i++) {
                name += passArray[i];
                name += "_";
            }
            name = name.substring(0, name.length()-1);
            result.setName(name.split(":")[0]);
            //endregion
        }

        return result;
    }

    public abstract void onError(Error error);

    public abstract void onError(Error error, String[] extraData);

    public abstract void onSuccess(Result result, boolean offline);

    private String decrypt(String inputString) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, InvalidKeySpecException {
        Cipher cipher1;
        String result = "";

        cipher1 = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        byte[] tempArr2 = null;
        int KEYSIZE = 1024;

        String privKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKmCsObjQcKw1OEnJp3ID8m4eual8K3I5dLmhHiYQpaJ4msBoNAmjmVvBCt/NgVYtlBL9gdDljzoQTE1DqCoSMGD3Q7LUxHJq0YSymNxgsoZDA3qO8b+LFMQgQaBdaNUXDAV1OXADaOZBUuMcsqFBEpjkpJrUpmdRGxzY2fCrXmxAgMBAAECgYBYw1T7Mpmm544x8JxqFu8dhHrm+JIVjpO6YneP5rtHtSBZRFr/ZJFAUGfwlwakuhWN6pkEqMGtVzl8GPp16mleAMZCrVsfyWOC4tZisSEq+EUpXZAU3tULWHmmjqktVfUiJtlcUe5GGSpLHjNTLzjSedeATpQAQcsDHnuj9leBWQJBAOhKLuubHbLDb47SaSOvGZWEIehIijZ5yYGCFvEW/AvcBjU/h5NoeNPVdORtLu+pLgHYUui2eTyl/x44xZS98LMCQQC60BAFc2+DTKkRmlU5ypaHUchUiFKV0qhASggiOP9ImgDChlXeoLiNamjU8Mj2u+jQ1OSPyANFfHL2BUW1uFYLAkEA22fmdagoHwe2QYLAyHHFipIprMuHsPjLukX0AXiiTVlfi8AcAMH3Dq9aH8B5SunTiO6ZMTHZxc1M8XDSPzJBPQJAfPZ28R6BxqO+KJ9DSVBCFyzWw+0YyU3L7I1ZGYNlCOyze2we/rgmG7rFe0PCOoICP8dZSPCzd4TC5d0q/17adwJAOA4PYkx+Pt7cyshDcDHUfq3l9oOwipqXFZ4T18SVOmyNzzieDid+QgRfgUQpEBuVaDNHe7ry9BzYX8YxRyPOqg==";

        PrivateKey privateKey = KeyFactory.getInstance("RSA")
                .generatePrivate(
                        new PKCS8EncodedKeySpec(Base64.decode(
                                privKey.getBytes(), Base64.DEFAULT)));
        cipher1.init(Cipher.DECRYPT_MODE, privateKey);
        int base64BlockSize = ((KEYSIZE / 8) % 3 != 0) ? (((KEYSIZE / 8) / 3) * 4) + 4
                : ((KEYSIZE / 8) / 3) * 4;
        int iterations = inputString.length() / base64BlockSize;
        ArrayList<Byte> arrayList = new ArrayList<Byte>();
        for (int i = 0; i < iterations; i++) {
            byte[] encryptedBytes = Base64.decode(inputString
                    .substring(base64BlockSize * i, base64BlockSize * i
                            + base64BlockSize), Base64.DEFAULT);

            byte[] arrTemp = reverse(encryptedBytes);
            byte[] decryptedBytes = cipher1.doFinal(arrTemp);
            for (int j = 0; j < decryptedBytes.length; j++) {
                if (decryptedBytes[j] != 0)
                    arrayList.add(decryptedBytes[j]);
            }
            tempArr2 = new byte[arrayList.size()];
            for (int j = 0; j < arrayList.size(); j++) {
                tempArr2[j] = arrayList.get(j);
            }
            result = new String(tempArr2, "UTF-8");

        }
        return result;
    }

    public byte[] reverse(byte[] array) {
        if (array == null) {
            return null;
        }
        int i = 0;
        int j = array.length - 1;
        byte tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
        return array;
    }

    private static class Location {
        public static int PETRA = 1;
        public static int BAPTISM = 15;
    }

    /**
     * Data contained in the ticket.
     * Dta contains: name, date, dateFormat, ticketType, ticketId, baptismEntry
     */
    public class Result {
        String name;
        String date;
        String dateFormat = "MM/dd/yyyy";
        int ticketType;
        int ticketId;
        boolean baptismEntry = false;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDateFormat() {
            return dateFormat;
        }

        public int getTicketId() {
            return ticketId;
        }

        public void setTicketId(int ticketId) {
            this.ticketId = ticketId;
        }

        public int getTicketType() {
            return ticketType;
        }

        public void setTicketType(int ticketType) {
            this.ticketType = ticketType;
        }

        public boolean getBaptismEntry() {
            return baptismEntry;
        }

        public void setBaptismEntry(boolean baptismEntry) {
            this.baptismEntry = baptismEntry;
        }

    }

}
