package com.jordanpass.jordanpass.library;

/**
 * Approval status. Can be {@code NOT_APPROVED}, {@code APPROVED}, or
 * {@code PENDING}.
 * 
 * @author Dalia
 * 
 */
public class ApprovalStatus {

	public static final int NOT_APPROVED = 0;
	public static final int APPROVED = 1;
	public static final int PENDING = 2;

}
