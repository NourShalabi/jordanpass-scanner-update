package com.jordanpass.jordanpass;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

/**
 * Runs when internet connection status changes. Starts {@link AutoSyncService}
 * when the device has an internet connection.
 * 
 */
public class InternetConnectionReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// Only run when it's a change in connectivity to internet
		if (!intent.getAction()
				.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)
				&& !intent.getAction().equals(
						ConnectivityManager.CONNECTIVITY_ACTION)) {
			// && !intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
			return;
		}

		ConnectivityManager cm = ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE));

		if (cm == null) {
			return;
		}

		// Is the device actually connected?
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isConnected()) {
			// Start the service unless it's running
			if (!isAutoSyncRunning(context)) {
				Intent pushIntent = new Intent(context, AutoSyncService.class);
				context.startService(pushIntent);
			}

			// Toast.makeText(context, "Start service",
			// Toast.LENGTH_SHORT).show();
		}
	}

	private boolean isAutoSyncRunning(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.jordanpass.jordanpass.AutoSyncService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

}