package com.jordanpass.jordanpass.library;

/**
 * Created by Dalia on 4/12/2016.
 */
public enum Error {
    ENCRYPTION_ERROR(),             //1. an error occurred, either encryption is wrong or scanner reading is wrong
    INVALID_TICKET_FORMAT(),        //2. ticket format is invalid
    INVALID_TICKET_FIELD(),         //3. ticket has an invalid/unparseable field
    OTHER(),                        //4. other error, unidentified
    NO_LOCATION_ACCESS(),           //5. ticket isn't allowed to access this location
    TICKET_ALREADY_USED_TODAY(),    //6.
    TICKET_ALREADY_USED(),          //7.
    TICKET_EXPIRED_FOR_LOCATION(),  //8.
    TICKET_EXPIRED_SINCE_ACTIVATION(), //9.
    INVALID_TICKET(),               //10. only if ticket isn't found on server
    TICKET_EXPIRED_YEAR();          //11. ticket has exceeded the one year period of use


    private String arabicString;
    private String englishString;
    private String[] arabicStrings = {
            "حصل خطأ غير متوقع. حاول مجددا او اتصل بالرقم +962795071188.",
            "حصل خطأ غير متوقع. حاول مجددا او اتصل بالرقم +962795071188.",
            "تذكرة غير صالحة. حاول مجددا او اتصل بالرقم +962795071188.",
            "حصل خطأ غير متوقع. حاول مجددا او اتصل بالرقم +962795071188.",
            "هذه التذكرة غير صالحة لهذا الموقع.",
            "تم استخدام هذه التذكرة اليوم في %s",
            "تم استخدام هذه التذكرة في %s",
            "انتهت صلاحية الدخول الى هذا الموقع فقط. أول دخول لهذا الموقع كان بتاريخ %s",
            "انتهت صلاحية هذه التذكرة بتاريخ %s",
            "تذكرة غير صالحة.",
            "إنتهت صلاحية هذه التذكرة بتاريخ %s"

    };
    private String[] englishStrings = {
            "An error occurred. Try again or contact +962795071188" ,
            "An error occurred. Try again or contact +962795071188" ,
            "The Ticket is invalid. Try again or contact +962795071188",
            "An error occured. Try again or contact +962795071188" ,
            "This ticket cannot be used in this location.",
            "This ticket was used today on %s",
            "This ticket was used on %s",
            "This ticket can no longer access this location. First entry for this location was on %s",
            "This ticket has expired on %s",
            "Invalid ticket.",
            "This ticket has expired on %s",
    };

    Error(){
        this.arabicString = arabicStrings[this.ordinal()];
        this.englishString = englishStrings[this.ordinal()];
    }

    public String getArabicString(){
        return arabicString;
    }

    public String getEnglishString(){
        return englishString;
    }
}
