package com.jordanpass.jordanpass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.jordanpass.jordanpass.library.ApprovalStatus;
import com.jordanpass.jordanpass.library.DatabaseHandler;
import com.jordanpass.jordanpass.library.Preferences;

/**
 * {@link IntentService} responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(Preferences.GCM_KEY);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
        long when = System.currentTimeMillis();

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context,
                SplashScreenActivity.class);
        // TODO set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);

        /*Notification notification = new Notification(R.drawable.ic_launcher,
                message, when);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notificationManager.notify(0, notification);*/

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);
        Notification notification = builder.setContentIntent(intent)
                .setSmallIcon(R.drawable.ic_launcher).setTicker(message).setWhen(when)
                .setAutoCancel(true).setContentTitle(title)
                .setContentText(message).build();

        notificationManager.notify(0, notification);
    }

    @Override
    protected void onError(Context arg0, String arg1) {
        // TODO an error, what next??
        Log.e("error", arg1);
        // Log.e("test", "AN ERROR OCCURED");
    }

    @Override
    protected void onMessage(Context arg0, Intent arg1) {
        SharedPreferences preferences = getSharedPreferences(
                Preferences.APP_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(arg0);
        String messageType = gcm.getMessageType(arg1);
        Bundle extras = arg1.getExtras();

        if (extras != null) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                Log.e("JordanPass", extras.toString());
            } else {
                if (arg1.hasExtra("time")) {
                    if (arg1.hasExtra("result")) {
                        // Compare send time of received message to last message
                        // received. If the current one is the newer one change
                        // ApprovalStatus according to the new message.

                        SimpleDateFormat formatter = new SimpleDateFormat(
                                "dd/MM/yyyy HH:mm:ss");
                        Calendar calendar = Calendar.getInstance(); // Get now time
                        Long time = calendar.getTimeInMillis();
                        try {
                            Date dateObj = formatter.parse(arg1
                                    .getStringExtra("time"));
                            time = dateObj.getTime(); // Get message time in
                            // millieseconds
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Long timeSaved = preferences.getLong(
                                Preferences.APPROVAL_TIME, time); // Time of last
                        // received
                        // message
                        if (time >= timeSaved) {
                            Boolean resultBool = arg1.getStringExtra("result")
                                    .equalsIgnoreCase("true");
                            Integer result;
                            if (resultBool)
                                result = ApprovalStatus.APPROVED;
                            else
                                result = ApprovalStatus.NOT_APPROVED;
                            edit.putInt(Preferences.APPROVAL_STATUS, result);
                            edit.putLong(Preferences.APPROVAL_TIME, time);
                            edit.commit(); // Commit the new values to preferences

                            generateNotification(arg0, "Your device has been "
                                    + (resultBool ? "approved."
                                    : "denied approval."));

                            Intent intent = new Intent("GCMMessage");
                            // add data
                            intent.putExtra("result", resultBool);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(
                                    intent);
                        }

                        // DONE: update approval status and app settings according
                        // to
                        // the result.
                        // DONE: Don't forget to remove approval if you get a
                        // message
                        // that says false.
                        // DONE: also broadcast to ApprovelPendingActivity to
                        // terminate and redirect to scanner.
                    } else if (arg1.hasExtra("command")) {
                        int command = Integer.parseInt(arg1
                                .getStringExtra("command"));
                        //int ALL_PASS = 0;
                        //int UNSYNCED_PASS = 1;
                        DatabaseHandler db = new DatabaseHandler(this);
                        switch (command) {
                            case 0:
                                db.deleteAllPasses();
                                break;
                            case 1:
                                db.deleteAllUnsyncedPasses();
                                break;
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onRegistered(Context arg0, String regId) {
        // DONE: Put GCM regId in app preferences

        SharedPreferences preferences = getSharedPreferences(
                Preferences.APP_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(Preferences.GCM_REG_ID, regId);
        edit.commit();
    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        // TODO Auto-generated method stub
        SharedPreferences preferences = getSharedPreferences(
                Preferences.APP_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(Preferences.GCM_REG_ID, "");
        edit.commit();

    }
}